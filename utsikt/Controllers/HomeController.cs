﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using utsikt.Helpers;

namespace utsikt.Controllers
{
    public class HomeController : BaseController
    {
        private const string Folder = "inbox";
        private const int ItemsPerPage = 3;
        private readonly JiraHelper _jira = new JiraHelper();

        public async Task<ActionResult> Index(int page = 1)
        {
            if (!Request.IsAuthenticated) return View();
            
            var messageCount = await GraphHelper.GetMessageCount(Folder);
            var pageCount = (messageCount.TotalItemCount ?? 0 + ItemsPerPage - 1) / ItemsPerPage;

            page = page <= pageCount && page > 0 ? page : 1;

            var offset = (page - 1) * ItemsPerPage;
            var messages = await GraphHelper.GetMessages(Folder, offset, ItemsPerPage);

            var formatted = messages
                .Select(message => GraphHelper.ConvertGraphMessage(message, _jira.GetIssues(message.Id)))
                .ToList();

            var currentMessages = new Models.Messages
            {
                UnreadItemCount = messageCount.UnreadItemCount ?? 0,
                TotalItemCount = messageCount.TotalItemCount ?? 0,
                PageCount = pageCount,
                CurrentPage = page,
                PreviousPage = page - 1,
                NextPage = page + 1,
                IsFirstPage = page == 1,
                IsLastPage = page == pageCount,
                IsHidePagination = messageCount.TotalItemCount <= ItemsPerPage,
                MessagesList = formatted,
                JiraIssueBaseAddress = ConfigurationManager.AppSettings["JiraIssueBaseAddress"]
            };

            return View(currentMessages);
        }

        public ActionResult Error(string message, string debug)
        {
            Flash(message, debug);
            return RedirectToAction("Index");
        }
    }
}