﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using utsikt.Helpers;
using utsikt.Models;

namespace utsikt.Views.Message
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly JiraHelper _jira = new JiraHelper();

        [HttpGet]
        public List<IssueMeta> Issues(string messageId)
        {
            return _jira.GetIssues(messageId);
        }

        [HttpPost]
        public async Task<ContentResult> Issues(IssueCreateRequest request)
        {
            var data = await _jira.CreateIssue(request.MessageId, request.Summary, request.Description);

            return Content(JsonConvert.SerializeObject(data), "application/json");
        }
    }
}