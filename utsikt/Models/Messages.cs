﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using System.Collections.Generic;

namespace utsikt.Models
{
    public class Messages
    {
        public int UnreadItemCount { get; set; }
        public int TotalItemCount { get; set; }
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }
        public int NextPage { get; set; }
        public int PreviousPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public bool IsHidePagination { get; set; }        
        public List<Message> MessagesList { get; set; }
        public string JiraIssueBaseAddress { get; set; }
    }
}