﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

namespace utsikt.Models
{
    public class IssueCreateRequest
    {
        public string MessageId { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
    }
}