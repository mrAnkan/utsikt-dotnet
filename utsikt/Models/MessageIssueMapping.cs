﻿

namespace utsikt.Models
{
    public class MessageIssueMapping
    {
        public string MessageId { get; set; }
        public string JiraKey { get; set; }
        public string JiraIdentifier { get; set; }
    }
}