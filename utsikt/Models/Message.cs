﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using Microsoft.Graph;
using System;
using System.Collections.Generic;

namespace utsikt.Models
{
    public class Message
    {
        public string Id { get; set; }
        public Recipient Sender { get; set; }
        public List<Recipient> ToRecipients { get; set; }
        public List<Recipient> CcRecipients { get; set; }
        public string Subject { get; set; }
        public bool IsRead { get; set; }
        public DateTimeOffset? ReceivedDateTime { get; set; }
        public string BodyPreview { get; set; }
        public string Body { get; set; }
        public string WebLink { get; set; }
        public List<IssueMeta> IssueKeys { get; set; } = new List<IssueMeta>();
    }
}