﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

namespace utsikt.Models
{
    public class IssueMeta
    {
        public string Key { get; set; }
        public string JiraIdentifier { get; set; }
    }
}