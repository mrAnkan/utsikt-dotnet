﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(utsikt.Startup))]

namespace utsikt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}