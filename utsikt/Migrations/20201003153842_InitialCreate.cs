﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace graph_tutorial.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MessageIssueMappings",
                columns: table => new
                {
                    MessageId = table.Column<string>(nullable: false),
                    JiraKey = table.Column<string>(nullable: false),
                    JiraIdentifier = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageIssueMappings", x => new { x.MessageId, x.JiraKey, x.JiraIdentifier });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessageIssueMappings");
        }
    }
}
