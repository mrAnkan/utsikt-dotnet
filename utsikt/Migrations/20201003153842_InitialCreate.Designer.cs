﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using utsikt.Database;

namespace graph_tutorial.Migrations
{
    [DbContext(typeof(UtsiktDatabaseContext))]
    [Migration("20201003153842_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8");

            modelBuilder.Entity("utsikt.Models.MessageIssueMapping", b =>
                {
                    b.Property<string>("MessageId")
                        .HasColumnType("TEXT");

                    b.Property<string>("JiraKey")
                        .HasColumnType("TEXT");

                    b.Property<string>("JiraIdentifier")
                        .HasColumnType("TEXT");

                    b.HasKey("MessageId", "JiraKey", "JiraIdentifier");

                    b.ToTable("MessageIssueMappings");
                });
#pragma warning restore 612, 618
        }
    }
}
