﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using utsikt.Models;

namespace utsikt.Helpers
{
    public class JiraHelper
    {
        private readonly Jira _client;
        private readonly string _jiraProject;
        
        public JiraHelper() : this(
            ConfigurationManager.AppSettings["JiraServer"], 
            ConfigurationManager.AppSettings["JiraUser"], 
            ConfigurationManager.AppSettings["JiraPassphrase"], 
            ConfigurationManager.AppSettings["JiraProject"])
        {
        }

        private JiraHelper(string server, string user, string password, string project)
        {
            _client = Jira.CreateRestClient(server, user, password);
            _jiraProject = project;
        }

        public List<IssueMeta> GetIssues(string messageId)
        {
            return new List<IssueMeta>();
        }

        public async Task<IssueMeta> CreateIssue(string messageId, string subject, string description)
        {
            var issue = _client.CreateIssue(_jiraProject);

            issue.Type = "Bug";
            issue.Priority = "Highest";
            issue.Summary = subject;
            issue.Description = description;
            
            var created = await issue.SaveChangesAsync();

            return new IssueMeta
            {
                Key = created.Key.ToString(),
                JiraIdentifier = created.JiraIdentifier
            };
        }
    }
}
