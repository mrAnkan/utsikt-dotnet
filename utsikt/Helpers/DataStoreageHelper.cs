﻿/**
* Copyright (c) Daniel Holm. All rights reserved.
* Copyright (c) Microsoft Corporation. All rights reserved.
* Licensed under the MIT license.
*/

using System;
using System.IO;

namespace utsikt.Helpers
{
    public class DataStoreageHelper
    {
        private readonly string _databasePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "utsikt.db");

        public DataStoreageHelper() 
        {
        }
    }
}