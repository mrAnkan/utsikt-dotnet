﻿/**
 * Copyright (c) Daniel Holm. All rights reserved.
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT license.
 */

using Microsoft.Graph;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using utsikt.TokenStorage;
using Microsoft.Identity.Client;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using utsikt.Models;

namespace utsikt.Helpers
{
    public static class GraphHelper
    {
        // Load configuration settings from PrivateSettings.config
        private static readonly string AppId = ConfigurationManager.AppSettings["ida:AppId"];
        private static readonly string AppSecret = ConfigurationManager.AppSettings["ida:AppSecret"];
        private static readonly string RedirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        private static readonly List<string> GraphScopes = new List<string>(ConfigurationManager.AppSettings["ida:AppScopes"].Split(' '));

        public static async Task<CachedUser> GetUserDetailsAsync(string accessToken)
        {
            var graphClient = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    (requestMessage) =>
                    {
                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", accessToken);
                        return Task.FromResult(0);
                    }));

            var user = await graphClient.Me.Request()
                .Select(u => new {
                    u.DisplayName,
                    u.Mail,
                    u.UserPrincipalName
                })
                .GetAsync();

            return new CachedUser
            {
                Avatar = string.Empty,
                DisplayName = user.DisplayName,
                Email = string.IsNullOrEmpty(user.Mail) ?
                    user.UserPrincipalName : user.Mail
            };
        }

        public static async Task<MailFolder> GetMessageCount(string folder)
        {
            var graphClient = GetAuthenticatedClient();

            return await graphClient.Me.MailFolders[$"{folder}"].Request()
                .Select("unreadItemCount,totalItemCount")
                .GetAsync();
        }

        public static async Task<IMailFolderMessagesCollectionPage> GetMessages(string folder, int offset, int itemPerPage)
        {
            var graphClient = GetAuthenticatedClient();

            return await graphClient.Me.MailFolders[$"{folder}"].Messages.Request()
                .Select("id,sender,toRecipients,ccRecipients,subject,isRead,receivedDateTime,bodyPreview,body,webLink")
                .Skip(offset)
                .Top(itemPerPage)
                .OrderBy("receivedDateTime DESC")
                .GetAsync();
        }

        public static Models.Message ConvertGraphMessage(Microsoft.Graph.Message message, List<IssueMeta> issueMeta)
        {
            return new Models.Message
            {
                Id = message.Id,  
                Sender = message.Sender, 
                ToRecipients = message.ToRecipients.ToList(),
                CcRecipients = message.CcRecipients.ToList(), 
                Subject = message.Subject,
                IsRead = message.IsRead ?? false,
                ReceivedDateTime = message.ReceivedDateTime,
                BodyPreview = message.BodyPreview, 
                Body = message.Body.Content, 
                WebLink = message.WebLink,
                IssueKeys = issueMeta
            };
        }

        private static GraphServiceClient GetAuthenticatedClient()
        {
            return new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (requestMessage) =>
                    {
                        var idClient = ConfidentialClientApplicationBuilder.Create(AppId)
                            .WithRedirectUri(RedirectUri)
                            .WithClientSecret(AppSecret)
                            .Build();

                        var tokenStore = new SessionTokenStore(idClient.UserTokenCache,
                            HttpContext.Current, ClaimsPrincipal.Current);

                        var accounts = await idClient.GetAccountsAsync();

                        // By calling this here, the token can be refreshed
                        // if it's expired right before the Graph call is made
                        var result = await idClient.AcquireTokenSilent(GraphScopes, accounts.FirstOrDefault())
                            .ExecuteAsync();

                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", result.AccessToken);
                    }));
        }
    }
}