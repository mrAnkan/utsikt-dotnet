# Configure Utsikt

1. Rename the `PrivateSettings.config.example` file to `PrivateSettings.config`.
2. Edit the `PrivateSettings.config` file
    1. Get the `AppID`, `AppSecret`, `RedirectUri`, `AppScopes` (Separated by spaces, the following scopes are needed: `"Mail.Read Mail.ReadWrite Mail.Send User.Read"`) from the **M-$oft Azure Active Directory admin center**
    2. Get the `JiraServer`, `JiraUser`, `JiraPassphrase`, `JiraProject` from your **Atlassian Jira instance**
3. Open `utsikt.sln` in Rider or Visual Studio. 
    1. In Visual Studio navigate to Solution Explorer, right-click the **utsikt** solution and choose **Restore NuGet Packages**.

## Register a web application with the Azure Active Directory admin center

1. Determine your ASP.NET applications's SSL URL. In Visual Studio's Solution Explorer, select the **utsikt** project. In the **Properties** window, find the value of **SSL URL**. Copy this value.


1. Open a browser and navigate to the [Azure Active Directory admin center](https://aad.portal.azure.com). Login using a **personal account** (aka: Microsoft Account) or **Work or School Account**.

1. Select **Azure Active Directory** in the left-hand navigation, then select **App registrations** under **Manage**.


1. Select **New registration**. On the **Register an application** page, set the values as follows.

    - Set **Name** to `ASP.NET Graph Tutorial`.
    - Set **Supported account types** to **Accounts in any organizational directory and personal Microsoft accounts**.
    - Under **Redirect URI**, set the first drop-down to `Web` and set the value to the ASP.NET app URL you copied in step 1.


1. Choose **Register**. On the **ASP.NET Graph Tutorial** page, copy the value of the **Application (client) ID** and save it, you will need it in the next step.


1. Select **Authentication** under **Manage**. Locate the **Implicit grant** section and enable **ID tokens**. Choose **Save**.


1. Select **Certificates & secrets** under **Manage**. Select the **New client secret** button. Enter a value in **Description** and select one of the options for **Expires** and choose **Add**.


1. Copy the client secret value before you leave this page. You will need it in the next step.

    > [!IMPORTANT]
    > This client secret is never shown again, so make sure you copy it now.
